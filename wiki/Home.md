Welcome 

Preferred position of utilizing private repo. 

The motivation behind private vaults is to spare your code without having it in the open. For example, programs that are restrictive for you right now and that you would prefer not to share. Successfully it's only a spot to back-up your private code in a remote repository.**** 
Practically 99% of all up-and-comers don't give any sign in their resume on the amount they can program or structure a program. Hell, some "Senior Java software engineers" I've met were confused to the point that they didn't realize what an interface is or why they would utilize it.
As to stresses that your code might need in the event that you distribute things transparently; you shouldn't fuss a lot about it. Simply having a record on github (at the composition minute) reveals to me that you're in a higher echelon of software engineers and I would say spotters just check the code quickly on what you've done. Despite the fact that you have a few slip-ups in your code it is as yet a superior check for your imminent business that you can really stuff, which matters more than anything.